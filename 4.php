<!DOCTYPE html>
<?php
$showSearchResult = 'hidden';
$showTime = 'hidden';
$error_message = '';
if(isset($_POST['search']))
{
    $search_results = [];
    $showError = 'hidden';
    $showTime = 'hidden';
    $error = false;
    if(isset($_POST['query'])){
        $start = microtime(true);
        $query = $_POST['query'];
        if(strlen($query) == 0){
            //handle error here!
            $showError = '';
            $error = true;
            $error_message = 'Please enter some query!';
        }
        else
        {
            $count = 1;
            $give_query = $query;
            $string_array = explode(' ', $query);
            foreach($string_array as $item)
            {
                if(strcmp($item, '') != 0 && strlen($result_query) != 0)
                {
                    $result_query = $result_query . '+' . $item;
                }
                else if(strcmp($item, '') != 0){
                    $result_query = $item;
                }
            }
            $api_call = "http://localhost:8983/solr/hw4_572/query?q=" . $result_query . "&json.limit=10";
            $json_data = @file_get_contents($api_call);
            // print($json_data);
            if($json_data === FALSE){
                //handle error here
                $showError = '';
                $error_message = 'Search returns no results';
            }
            else{
                $decoded_data = json_decode($json_data,true);
                if($decoded_data['status'] != 0)
                {
                    $showError = '';
                    $error_message = 'Status return non zero, please check network connections!';
                }
                else if(sizeof($decoded_data['response']) == 0)
                {
                    $showError = '';
                    $error_message = 'Status return non zero, please check network connections!';
                }
                else{
                    // echo "has results here!";
                    if(intval($decoded_data["response"]['numFound']) == 0){
                        // echo "handle no results";
                        $showSearchResult = 'hidden';
                        $showError = '';
                        $error_message = 'Search returns no results';
                    }
                    else{
                        //handle data
                        // ($decoded_data);
                        foreach($decoded_data["response"]["docs"] as $data){
                            // print($count);
                            // print(", ");
                            // print($data["title"][0]);
                            // print("\n");
                            $count += 1;
                            $search_results[$count][0] = $data["title"][0];
                            $search_results[$count][1] = $data["og_url"][0];
                            $search_results[$count][2] = $data["id"];
                            if(array_key_exists("og_description",$data)){
                                $search_results[$count][3] = $data["og_description"][0];
                            }
                            else{
                                $search_results[$count][3] = "N/A";
                            }
                        }
                        $showSearchResult = '';
                    }
                    // print("eher2");
                }
                // print(file_get_contents($api_call));
                $showTime = '';
                $end_time = microtime(true); 
                $time_used = $end_time-$start;
            }
        }
    }
}

?>
<html>
<style>
.result_container
{
    margin: auto;
    width: 80%;
    height: auto;
    align-content: center;
}

.time_used
{
    margin: auto;
    width: 80%;
    height: auto;
    align-content: center;   
    color: grey;
}

.Additional_Container
{
    background-color: lightgray;
    margin: auto;
    width: 30%;
    height: 40%;
}

.Additional_Container #error
{
    font-style: italic;
    font-size: 30px;
    text-align: center;
    border-color: grey;
}
</style>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>CSCI 572 HW4</title>
  </head>
  <body>
    <h1 align="center" margin-top: "auto" margin-left: "auto" >Solr results from LATimes</h1>
    <form action = "" method = "post" id = 'search_form'>
        <div class = "Input_Container" align = "center">
            <input type = "text" name = "query" id = "query" <?php echo 'value = "'. $give_query .'"';?>>
        </div>
        <div class = "Submittion_Container" align = "center">
            <input type="submit" name = "search" value = "Search">
            <input type="submit" name = "clear" value = "Clear">
        </div>
        <div class = "Additional_Container" <?php echo $showError; ?> >
            <p id = "error" name = "error_message"><?php echo $error_message ?></p>
		</div>
        <div class = "result_container"  <?php echo $showSearchResult; ?> id = 'temperature_container_enabled'>
            <table style = "width: 100%">
                <tr>
                    <th>Title</th>
                    <th>URL</th>
                    <th>ID</th>
                    <th>Description</th>
                </tr>
                <?php
                    for($i = 0; $i < $count; ++$i)
                    {
						echo "<tr>";
                        echo "<td><a href = '" . $search_results[$i][1] . "'>";
						echo $search_results[$i][0];
						echo "</a></td>";
                        // echo "<td href='". $search_results[$i][1] ."'>";
                        echo "<td><a href = '" . $search_results[$i][1] . "'>";
						echo $search_results[$i][1];
						echo "</a></td>";
						echo "<td>";
						echo $search_results[$i][2];
                        echo "</td>";
                        echo "<td>";
						echo $search_results[$i][3];
						echo "</td>";
                        echo "</tr>";
                    }
                  ?>
            </table>
		</div>
        <div class = "time_used" align = "center" <?php echo $showTime; ?>>
            <p id = "time" name = "time_message"><?php echo "Time used: ";echo $time_used ?></p>
		</div>
    </form>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>